from django.db import models


class producto_tienda(models.Model):
    'un producto de la tienda'
    # nombre
    nombre = models.TextField(max_length=100, help_text='nombre del producto')
    # precio
    precio = models.IntegerField()
    # costo
    costo = models.IntegerField()

    class Meta:
        abstract = True

    def __str__(self):
        # mostrar nombre
        return self.nombre


class mascota(producto_tienda):
    'mascotas para vender'
    # especie
    # raza
    # descripcion

    def __str__(self):
        # mostrar especie y raza
        return


class alimento(producto_tienda):
    'alimentos para vender'
    # marca
    # especie
    # descripcion

    def __str__(str):
        # mostrar nombre y marca
        return
