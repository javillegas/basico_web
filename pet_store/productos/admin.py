"""
definir que queremos que sea visible en el administrador
"""
from django.contrib import admin

# llamar los modelos para que se vean en el administrador
from .models import alimento, mascota

# registrar los modelos en el administrador
admin.site.register()
admin.site.register()
