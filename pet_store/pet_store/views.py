from django.shortcuts import render_to_response


def indexView(request):
    """function for generate a view for the main page of the app"""
    c = {
        'title': 'Tienda online de mascotas',
        }
    return render_to_response('index_view.html', c)
