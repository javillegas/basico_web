# instalar django
pip install django
# crear proyecto django
django-admin startproject pet_store
# hacer apps
cd pet_store
./manage.py startapp productos
./manage.py startapp clientes
# crear modelos
# installar apps
# registrar apps en django admin
# migrar modelos
./manage.py makemigrations
./manage.py migrate
# crear superusuario
./manage.py createsuperuser
# correr aplicacion
./manage.py runserver 0.0.0.0:8080
# mirar la página
localhost:8080
# mirar el administrador
localhost:8080/admin
# definir urls
# crear vistas
